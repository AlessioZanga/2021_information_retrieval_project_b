import re
import string
import logging

from tqdm import tqdm
from multiprocessing import Pool, cpu_count
from sklearn.feature_extraction.text import TfidfVectorizer

from dataclasses import dataclass
from typing import Dict, List

import nltk

nltk.download('stopwords')
nltk.download('wordnet')

from nltk.tokenize import WordPunctTokenizer
from nltk.stem.snowball import SnowballStemmer

from .wikipage import Wikipage


@dataclass
class Wikitopic:

    label: str
    terms: List[str]

    @staticmethod
    def _preprocessing(page: Wikipage) -> List[str]:
        # Text to lower case
        text = page.abstract.lower()
        # Tokenize text
        text = WordPunctTokenizer().tokenize(text)
        # Remove puctuation
        pattern = re.compile("[" + string.punctuation + "]+")
        text = [term for term in text if not pattern.fullmatch(term)]
        # Remove decimal digits
        text = [term for term in text if not bool(re.search(r"\d", term))]
        # Remove roman digits
        pattern = re.compile(r"^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$")
        text = [term for term in text if not pattern.match(term.upper())]
        # Remove stop words
        text = [term for term in text if term not in nltk.corpus.stopwords.words('english')]
        # Lemmatization
        stemmer = SnowballStemmer("english")
        text = [stemmer.stem(term) for term in text]
        # Join back each terms to a sigle string
        text = ' '.join([str(term) for term in text])
        return text

    @staticmethod
    def _terms_from_tf_idf(pages: List[str], limit: int) -> List[str]:
        # Set to return only the k greatest word by term frequency across the corpus
        vectorizer = TfidfVectorizer(max_features=limit)
        vectorizer.fit_transform(pages)
        return vectorizer.get_feature_names()

    @classmethod
    def from_pages(cls, label: str, pages: List[Wikipage], limit: int = 15):
        logging.warn(f"Processing pages for topic '{label}' terms extraction")
        pool = Pool(cpu_count())
        pages = [page for page in tqdm(pool.imap(cls._preprocessing, pages), total=len(pages))]
        pool.close()
        terms = cls._terms_from_tf_idf(pages, limit)
        return cls(label, terms)
