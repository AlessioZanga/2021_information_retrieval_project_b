import re
import mwparserfromhell as wikiparser

from functools import cached_property
from typing import List


class Wikipage:

    _text: str

    title: str
    topics: List[str]
    is_disambiguation: bool
    is_redirect: bool

    def __init__(self, xml: object):
        # Get text from xml object
        self._text = xml.find("revision").find("text").text

        # Extract attributes
        self.title = xml.find("title").text
        self.topics = re.findall(r"\[\[Category:([^\]]+)", self._text)
        self.topics = [topic.split("|")[0] for topic in self.topics]
        self.is_disambiguation = ("(disambiguation)" in self.title)
        self.is_redirect = ("#REDIRECT" in self._text)

        # Retain only first paragraph
        self._text = self._text.split("==")[0]
    
    @cached_property
    def abstract(self) -> str:
        abstract = wikiparser.parse(self._text, skip_style_tags=True)
        return abstract.strip_code().replace("\n", "")
    
    def __repr__(self) -> str:
        return f"""Wikipage(title={self.title}, topics={self.topics}, is_disambiguation={self.is_disambiguation}, is_redirect={self.is_redirect})"""
