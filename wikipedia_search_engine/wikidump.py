import os
import bz2
import pickle
import logging
import hashlib
import requests

from os.path import abspath, dirname, exists, isfile, join, pardir
from tqdm import tqdm
from typing import List
import xml.etree.cElementTree as ET

from .wikipage import Wikipage


class Wikidump:

    pages: List[Wikipage]

    def __init__(self, multistream: str, index: str):
        multistream = self._download(multistream)
        index = self._download(index)
        # Load from cache if exists
        key = multistream + index
        key = hashlib.md5(key.encode()).hexdigest()
        key = multistream + key + ".bin"
        logging.warn(f"Check if cache exists")
        if isfile(key):
            logging.warn("Cache found, load pages from cache")
            with open(key, "rb") as file:
                self.pages = pickle.load(file)
        else:
            logging.warn("Cache not found, load pages from dumps")
            # Read and decompress index from file
            with open(index, "rb") as file:
                index = bz2.decompress(file.read()).decode()
            # For each line get the offset
            offsets = {line.split(":")[0] for line in index.split("\n")}
            # Map offsets to sorted integers
            offsets = list(sorted([int(offset) for offset in offsets if len(offset) > 0]))
            # Create consecutive begin-end offset pairs
            offsets = list(zip(offsets, offsets[1:]))
            # Extract streams incrementally using offtest pairs
            self.pages = []
            for (begin, end) in tqdm(offsets):
                with open(multistream, "rb") as file:
                    file.seek(begin)
                    stream = bz2.decompress(file.read(end-begin))
                stream = ET.fromstring("<root>" + stream.decode() + "</root>")
                stream = [Wikipage(page) for page in stream.findall("page")]
                self.pages.extend(stream)
            # Save cache
            with open(key, "wb") as file:
                pickle.dump(self.pages, file)

    def _download(self, url: str) -> str:
        # Create cache folder
        path = join(dirname(abspath(__file__)), pardir, "cache")
        os.makedirs(path, exist_ok=True)
        # Check if file already download
        path = join(path, url.split("/")[-1])
        if not exists(path):
            logging.warn(f"Download file from {url}")
            response = requests.get(url, stream=True)
            size = int(response.headers.get("content-length", 0))
            bar = tqdm(total=size, unit="iB", unit_scale=True)
            with open(path, "wb") as file:
                for data in response.iter_content(1024):
                    bar.update(len(data))
                    file.write(data)
            bar.close()
        else:
            logging.warn(f"File from {url} already downloaded")
        return path
