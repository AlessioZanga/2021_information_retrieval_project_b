import os
import json
import pickle
import logging
import hashlib
import requests

import pandas as pd

from os.path import abspath, dirname, isfile, join, pardir
from bs4 import BeautifulSoup
from collections import defaultdict, deque
from elasticsearch import Elasticsearch
from elasticsearch.helpers import parallel_bulk
from tqdm import tqdm
from typing import List

from .wikidump import Wikidump
from .wikipage import Wikipage
from .wikitopic import Wikitopic


class Wikipedia:

    pages: List[Wikipage]
    engine: Elasticsearch

    index_name: str
    index_conf: str

    def __init__(self, pages: List[Wikipage]):
        # Loade pages
        self.pages = pages
        # Create ES instance
        logging.warn("Create ElasticSearch engine")
        self.engine = Elasticsearch()
        self.index_name = "wikipedia"
        self.index_conf = {
            "settings": {
                "number_of_shards": 1,
                "number_of_replicas": 1,
                "analysis": {
                    "filter": {
                        "digit_filter": {
                            "type": "keep_types",
                            "types": "<NUM>",
                            "mode": "exclude"
                        },
                        "english_stop": {
                            "type": "stop",
                            "stopwords": "_english_"
                        },
                        "english_stemmer": {
                            "type": "stemmer",
                            "language": "english"
                        },
                        "english_synonyms": {
                            "type": "synonym",
                            "format": "wordnet",
                            "synonyms": self.get_synonyms()
                        }
                    },
                    "analyzer": {
                        "synonym_english": {
                            "tokenizer": "standard",
                            "filter": [
                                "english_synonyms",
                                "english_stemmer",
                                "lowercase",
                                "english_stop",
                                "digit_filter",
                            ]
                        }
                    }
                }
            },
            "mappings": {
                "properties": {
                    "title": {
                        "type": "text",
                        "analyzer": "synonym_english"
                    },
                    "abstract": {
                        "type": "text",
                        "analyzer": "synonym_english"
                    },
                    "topic": {
                        "type": "text",
                        "boost": 2
                    }
                }
            }
        }
        # Reset index if exists
        logging.warn(f"Check if ES index '{self.index_name}' exists")
        if self.engine.indices.exists(index=self.index_name):
            logging.warn(f"Delete ES index '{self.index_name}'")
            self.engine.indices.delete(index=self.index_name)
        # Create index using name and configs
        logging.warn(f"Create ES index '{self.index_name}'")
        self.engine.indices.create(index=self.index_name, body=self.index_conf)

    def index(self, topics: List[str] = None):
        # Get pages by topics if any
        pages = self.pages
        if topics is not None:
            pages = [
                page for topic in topics
                for page in self.get_pages_by_topic(topic)
            ]
        # Define data generator
        def data():
            conf = self.index_conf["mappings"]["properties"]
            for page in tqdm(pages):
                yield {
                    "_op_type": "index",
                    "_index": self.index_name,
                    "_source": {
                        k: getattr(page, k)
                        for k in conf.keys()
                    }
                }
        # Execute parallel bulk indexing
        logging.warn(f"Index wikipedia pages")
        deque(parallel_bulk(self.engine, data()), maxlen=0)
        # Refresh index after indexing
        self.engine.indices.refresh(index=self.index_name)

    def get_pages_by_topic(self, topic: str) -> List[Wikipage]:
        pages = [page for page in self.pages if topic in page.topics]
        # Select a single topic from the associated topic list
        for page in pages:
            page.topic = topic
        return pages

    def get_terms_by_topic(self, topic: str) -> Wikitopic:
        return Wikitopic.from_pages(topic, self.get_pages_by_topic(topic))
    
    def get_synonyms(self, path: str = "synonyms.json") -> List[str]:
        path = join(dirname(abspath(__file__)), path)
        with open(path, "r") as file:
            synonyms = json.load(file)
        return synonyms

    def search(self, *args, **kwargs):
        # Init query
        query = defaultdict(list)
        # Get query fields
        fields = list(self.index_conf["mappings"]["properties"].keys())
        # Check input fields
        if len(kwargs.keys() - set(fields + ["_type", "_syno"])) > 0:
            raise IndexError("Input field do not exists")
        # Fill query fields using input
        if args:
            for k in fields:
                for v in args:
                    query[k].extend(v.split(" "))
        if kwargs:
            for k, v in kwargs.items():
                if k in fields:
                    query[k].extend(v.split(" "))
        # Choose query type
        _syno = kwargs.get("_syno", False)
        _type = kwargs.get("_type", "string")
        _type = _type if not _syno else "string"
        _type = getattr(self, "_search_" + _type, self._search_string)
        # Build query
        query = _type(query, _syno=_syno)
        # Execute query
        query = self.engine.search(body=query, index=self.index_name)
        query = query["hits"]["hits"]
        # Format query results
        query = [
            {
                "score": q["_score"],
                "title": q["_source"]["title"],
                "abstract": q["_source"]["abstract"],
                "topic": q["_source"]["topic"]
            }
            for q in query
        ]
        return pd.DataFrame(query)

    def _search_bool(self, query, **kwargs):
        query = [{"match": {k: v}} for k, values in query.items() for v in values]
        query = {"query": {"bool": {"should": query}}}
        return query

    def _search_string(self, query, **kwargs):
        query = [f"{k}:({' '.join(v)})" for k, v in query.items()]
        query = {
            "query": " OR ".join(query),
            "auto_generate_synonyms_phrase_query": kwargs["_syno"],
        }
        query = {"query": {"query_string": query}}
        return query

    @classmethod
    def from_dumps_url(
        cls,
        url: str = "https://dumps.wikimedia.org/enwiki/20201120/",
        limit: int = 1,
        remove_disambiguation: bool = True,
        remove_redirect: bool = True,
    ):
        # Check download cache
        key = hashlib.md5(url.encode()).hexdigest() + ".bin"
        key = join(dirname(abspath(__file__)), pardir, "cache", key)
        # Load from cache if exists
        if isfile(key):
            logging.warn("Cache found, load dumps from cache")
            with open(key, "rb") as file:
                dumps = pickle.load(file)
        else:
            # Get multistream-index pairs
            logging.warn(f"Get dumps urls from {url} with {limit} multistream limit")
            dumps = requests.get(url)
            dumps = BeautifulSoup(dumps.content, "html.parser")
            dumps = [link.get("href") for link in dumps.find_all("a")]
            # Remove first non-incremental multistream-index pair
            dumps = [
                "https://dumps.wikimedia.org/" + href
                for href in dumps
                if "multistream" in href
            ][2:]
            # Create multistream-index pairs
            dumps = list(zip(dumps, dumps[1:]))
            # Create cache dir
            os.makedirs(os.path.dirname(key), exist_ok=True)
            # Save cache
            with open(key, "wb") as file:
                pickle.dump(dumps, file)
        # Download multistream-index pairs
        dumps = [
            Wikidump(multistream, index).pages
            for multistream, index in dumps[:limit]
        ]
        # Return pages
        dumps = [
            page for pages in dumps for page in pages
            if not (
                (remove_disambiguation and page.is_disambiguation) or
                (remove_redirect and page.is_redirect)
            )
        ]
        return cls(dumps)
