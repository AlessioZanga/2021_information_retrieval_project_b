# Information Retrieval - 2020/2021 - Project B

## How to Install

Clone the repository:

    git clone https://gitlab.com/AlessioZanga/2021_information_retrieval_project_b.git

Change directory:

    cd 2021_information_retrieval_project_b

Install the Python dependencies:

    pip install -r requirements.txt

The package can be imported directly in any (Python >= 3.7) interpreter:

    import wikipedia_search_engine as wse

## How to Use

For a step-by-step usage, please refer to the *demo.ipynb* Jupyter Notebook in the root folder.
